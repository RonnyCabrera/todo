//
//  ViewController.swift
//  ToDo
//
//  Created by RonnyCabrera on 8/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDataSource, UITabBarDelegate {

    let itemManager = ItemManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let item1 = Item(title: "To Do 1", location: "Office", description: "Do Somehting")
        let item2 = Item(title: "To Do 2", location: "House", description: "Feed the Cat")
        let item3 = Item(title: "To Do 3", location: "Brasil", description: "Go to the beach")
        let item4 = Item(title: "To Do 4", location: "Narnia", description: "Eat")
        
        itemManager.toDoItem = [item1, item2, item3]
        itemManager.doneItem = [item4]
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItem.count
        }
        return itemManager.doneItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if indexPath.section == 0 {
            cell.textLabel?.text = itemManager.toDoItem[indexPath.row].title
        }
        else {
            cell.textLabel?.text = itemManager.doneItem[indexPath.row].title
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "To DO" : "Done"
    }
}

