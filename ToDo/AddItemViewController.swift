//
//  AddItemViewController.swift
//  ToDo
//
//  Created by Ronny Cabrera on 9/5/18.
//  Copyright © 2018 RonnyCabrera. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextFiled: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextFiled: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextFiled.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextFiled.text ?? ""
        
        let item = Item(
            title: itemTitle,
            location:itemLocation,
            description:itemDescription
        )
        
        guard itemTitle != "" else {
            print ("Insertar Titulo")
            return
        }
        guard itemLocation != "" else {
            print ("Insertar Locacion")
            return
        }
        guard itemDescription != "" else {
            print ("Insertar Descripcion")
            return
        }
        print("Registrando nuevo Item ")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
